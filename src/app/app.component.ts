import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
declare const gtag: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'buscacode-front';

  constructor(
    private router: Router,
  ) {
    const navEndEvents$ = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      );

    navEndEvents$.subscribe({
      next: (event: any) => {
        if (localStorage.getItem('cookie') === 'true') {
          gtag('config', 'G-WCYV8H22DQ', {
            'page_path': event.urlAfterRedirects
          });
        }
      }
    });

  }

}

