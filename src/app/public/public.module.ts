import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { NavBarComponent } from './layouts/nav-bar/nav-bar.component';
import { CookiesMessageComponent } from './components/cookies-message/cookies-message.component';


@NgModule({
  declarations: [
    MainPageComponent,
    NavBarComponent,
    CookiesMessageComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule
  ]
})
export class PublicModule { }
