import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
declare const gtag: any;
declare let dataLayer: any;

@Component({
  selector: 'app-cookies-message',
  templateUrl: './cookies-message.component.html',
  styleUrls: ['./cookies-message.component.css']
})
export class CookiesMessageComponent implements OnInit {

  @ViewChild('CookieSection') sectionCookie!: ElementRef;
  @ViewChild('CookieNo') cookieNo!: ElementRef;
  @ViewChild('CookieYes') cookieYes!: ElementRef;
  @ViewChild('CookieScripts') nuevosScripts!: ElementRef;


  private urlsScriptsCookies = ['https://www.googletagmanager.com/gtag/js?id=G-WCYV8H22DQ'];

  public hasLocalStorage = false;
  public cookiesHasAnswer = false;

  get isDisplayCookieMessage() {
    return this.hasLocalStorage && !this.cookiesHasAnswer;
  }

  constructor() { }

  ngOnInit(): void {
    if (localStorage) {
      this.hasLocalStorage = true;
      this.verifyCookies();
    }
  }

  public contentScriptsCookies() {

    // Google Tag Manager
    window.dataLayer = window.dataLayer || [];
    function gtag(_: any, __: any) { dataLayer.push(arguments); }
    gtag('js', new Date());

    /* gtag('config', 'G-WCYV8H22DQ'); */
    // Fin Google Tag Manager

  }

  removeCookieMessage() {
    const seccionCookie = this.sectionCookie.nativeElement;
    seccionCookie.remove();
  }

  acceptCookies() {
    // Oculta el HTML de cookies
    this.removeCookieMessage();
    // Guarda que ha aceptado
    localStorage.setItem('cookie', 'true');
    // Tu codigo a ejecutar si aceptan las cookies
    this.onYesCookies();
  }


  onYesCookies() {
    const nuevosScripts = this.nuevosScripts.nativeElement;
    if (document) {
      // Crea los <script>
      this.urlsScriptsCookies.forEach((url) => {
        const nuevoScript = document.createElement('script');
        nuevoScript.setAttribute('src', url);
        nuevosScripts.appendChild(nuevoScript);
      });
      // Lanza los códigos
      this.contentScriptsCookies();
    }
  }

  rejectCookies() {
    // Oculta el HTML de cookies
    this.removeCookieMessage();
    // Guarda que ha aceptado
    localStorage.setItem('cookie', 'false');

  }


  verifyCookies() {
    // Comprueba si en el pasado el usuario ha marcado una opción
    if (localStorage.getItem('cookie') !== null) {
      this.cookiesHasAnswer = true;
      if (localStorage.getItem('cookie') === 'true') {
        // Aceptó
        this.acceptCookies();
      } else {
        // No aceptó
        this.rejectCookies();
      }
    }
  }




}
