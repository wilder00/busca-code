import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BunkerAlfaPageComponent } from './pages/bunker-alfa-page/bunker-alfa-page.component';
import { BunkersRoutingModule } from './bunkers-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    BunkerAlfaPageComponent
  ],
  imports: [
    CommonModule,
    BunkersRoutingModule,
    FlexLayoutModule,
  ]
})
export class BunkersModule { }
