import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { interval, take } from 'rxjs';
import { LinkService } from 'src/app/services/link.service';
import { generalSeo, ogSeo } from 'src/seo.info';
import { BunkerCode } from '../../models/bunker-code.model';
import { AlfaCodeService } from '../../services/alfa-code.service';

@Component({
  selector: 'app-bunker-alfa-page',
  templateUrl: './bunker-alfa-page.component.html',
  styleUrls: ['./bunker-alfa-page.component.scss']
})
export class BunkerAlfaPageComponent implements OnInit, AfterViewInit {

  @ViewChild('bcCode') bcCode!: ElementRef;

  public title: string = "Código Bunker Alfa | BuscaCode";
  public bunkerCode!: BunkerCode;

  public wasRendered = false;

  constructor(
    private alfaCodeService: AlfaCodeService,
    private render: Renderer2,
    private linkService: LinkService,
    private metaTagService: Meta,
    private titleService: Title,
  ) {
    this.linkService.addTag({ rel: 'canonical', href: 'https://buscacode.com/bunkers/bunker-alfa' });
  }

  ngOnInit(): void {
    this._setPageMetaInfo();
    this._subscribeToAlfaCode();
  }

  ngAfterViewInit(): void {
    this._renderCode()
  }

  private _renderCode() {
    let count = 0;
    let codeText = "";
    interval(350).pipe(take(5)).subscribe({
      next: (number: number) => {
        codeText = codeText + this.bunkerCode.password[number];
        this.bcCode.nativeElement.innerText = codeText;
      }
    });
  }

  private _subscribeToAlfaCode() {
    this.alfaCodeService.getActualBunkerCode().subscribe({
      next: (response: BunkerCode) => {
        this.bunkerCode = response
      },
      error: (error) => {
        console.error("Error obteniendo Código: ", error);
      }
    })
  }




  _setPageMetaInfo() {
    this.titleService.setTitle(this.title);
    //General Tags
    this.metaTagService.addTags([
      { name: 'keywords', content: 'código bunker alfa en last day on earth, bunker alfa last day on earth' },
      { name: 'robots', content: generalSeo.robots },
      { name: 'author', content: generalSeo.author },
      { name: 'viewport', content: generalSeo.viewport },
      { name: 'date', content: generalSeo.date, scheme: generalSeo.dateSchema },
      { name: 'language', content: generalSeo.language },
    ])

    //General Tags
    this.metaTagService.addTags([
      { name: 'og:url', content: "https://buscacode.com/bunkers/bunker-alfa" },
      { name: 'og:type', content: ogSeo.type },
      { name: 'og:title', content: this.title },
      { name: 'og:description', content: ogSeo.description },
      { name: 'og:image', content: ogSeo.image },
      { name: 'og:site_name', content: ogSeo.siteName },
    ])
  }


}
