import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BunkerAlfaPageComponent } from './bunker-alfa-page.component';

describe('BunkerAlfaPageComponent', () => {
  let component: BunkerAlfaPageComponent;
  let fixture: ComponentFixture<BunkerAlfaPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BunkerAlfaPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BunkerAlfaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
