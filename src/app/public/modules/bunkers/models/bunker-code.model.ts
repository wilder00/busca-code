export interface BunkerCode {
  id?: number;
  password: string;
  startDate: string;
  endDate: string;
}