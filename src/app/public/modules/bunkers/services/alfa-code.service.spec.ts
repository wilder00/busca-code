import { TestBed } from '@angular/core/testing';

import { AlfaCodeService } from './alfa-code.service';

describe('AlfaCodeService', () => {
  let service: AlfaCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlfaCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
