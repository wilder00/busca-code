import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BunkerCode } from '../models/bunker-code.model';

@Injectable({
  providedIn: 'root'
})
export class AlfaCodeService {

  private _bunkerCodes: BunkerCode[] = [
    {
      id: 1,
      password: '11895',
      startDate: new Date('2022-04-03T19:00:00-05:00').toUTCString(),
      endDate: new Date("2022-04-05T18:59:59-05:00").toUTCString(),
    },
    {
      id: 2,
      password: '12273',
      startDate: new Date("2022-04-05T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-07T18:59:59-05:00").toUTCString(),
    },
    {
      id: 3,
      password: '20624',
      startDate: new Date("2022-04-07T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-09T18:59:59-05:00").toUTCString(),
    },
    {
      id: 4,
      password: '08815',
      startDate: new Date("2022-04-09T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-11T18:59:59-05:00").toUTCString(),
    },
    {
      id: 5,
      password: '82202',
      startDate: new Date("2022-04-11T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-13T18:59:59-05:00").toUTCString(),
    },
    {
      id: 6,
      password: '26942',
      startDate: new Date("2022-04-13T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-15T18:59:59-05:00").toUTCString(),
    },
    {
      id: 7,
      password: '68769',
      startDate: new Date("2022-04-15T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-17T18:59:59-05:00").toUTCString(),
    },
    {
      id: 8,
      password: '82256',
      startDate: new Date("2022-04-17T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-19T18:59:59-05:00").toUTCString(),
    },
    {
      id: 9,
      password: '29130',
      startDate: new Date("2022-04-19T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-21T18:59:59-05:00").toUTCString(),
    },
    {
      id: 10,
      password: '97045',
      startDate: new Date("2022-04-21T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-23T18:59:59-05:00").toUTCString(),
    },
    {
      id: 11,
      password: '72450',
      startDate: new Date("2022-04-23T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-25T18:59:59-05:00").toUTCString(),
    },
    {
      id: 12,
      password: '21627',
      startDate: new Date("2022-04-25T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-27T18:59:59-05:00").toUTCString(),
    },
    {
      id: 13,
      password: '10521',
      startDate: new Date("2022-04-27T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-04-29T18:59:59-05:00").toUTCString(),
    },
    {
      id: 14,
      password: '04396',
      startDate: new Date("2022-04-29T19:00:00-05:00").toUTCString(),
      endDate: new Date("2022-05-01T18:59:59-05:00").toUTCString(),
    }
  ];

  constructor() { }

  public getActualBunkerCode(): Observable<BunkerCode> {
    let alfaCode = this._bunkerCodes.find((code) => {
      let startDate = new Date(code.startDate);
      let endDate = new Date(code.endDate);
      let nowDate = new Date(Date.now());
      let isEqualsOrMoreThanStartDate = startDate.getTime() <= nowDate.getTime();
      let isEqualsOrLessThanEndDate = nowDate.getTime() <= endDate.getTime();
      console.log("StartDate: ", code.startDate);
      console.log("StartDate Date: ", startDate, startDate.toISOString());
      console.log("StartDate Date toLocalString: ", startDate.toLocaleDateString());
      console.log("now: ", new Date(Date.now()).toISOString());

      return isEqualsOrMoreThanStartDate && isEqualsOrLessThanEndDate;
    })!;

    return of(alfaCode);
  }
}

