import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BunkerAlfaPageComponent } from './pages/bunker-alfa-page/bunker-alfa-page.component';

const routes: Routes = [
  {
    path: 'bunker-alfa',
    component: BunkerAlfaPageComponent,
  },
  {
    path: '',
    redirectTo: 'bunker-alfa'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BunkersRoutingModule { }
