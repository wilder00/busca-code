import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { LinkService } from 'src/app/services/link.service';
import { generalSeo, ogSeo } from 'src/seo.info';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  constructor(
    private linkService: LinkService,
    private metaTagService: Meta,
    private titleService: Title,
  ) {
    this.linkService.addTag({ rel: 'canonical', href: 'https://buscacode.com/' });
  }

  ngOnInit(): void {
    this._setPageMetaInfo();
  }

  _setPageMetaInfo() {
    this.titleService.setTitle(generalSeo.title);
    //General Tags
    this.metaTagService.addTags([
      { name: 'keywords', content: generalSeo.keywords },
      { name: 'robots', content: generalSeo.robots },
      { name: 'author', content: generalSeo.author },
      { name: 'viewport', content: generalSeo.viewport },
      { name: 'date', content: generalSeo.date, scheme: generalSeo.dateSchema },
      { name: 'language', content: generalSeo.language },
    ])

    //General Tags
    this.metaTagService.addTags([
      { name: 'og:url', content: ogSeo.url },
      { name: 'og:type', content: ogSeo.type },
      { name: 'og:title', content: ogSeo.title },
      { name: 'og:description', content: ogSeo.description },
      { name: 'og:image', content: ogSeo.image },
      { name: 'og:site_name', content: ogSeo.siteName },
    ])
  }

}
