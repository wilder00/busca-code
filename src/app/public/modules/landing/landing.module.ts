import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { HeroSectionComponent } from './components/sections/hero-section/hero-section.component';


@NgModule({
  declarations: [
    MainPageComponent,
    HeroSectionComponent
  ],
  imports: [
    CommonModule,
    LandingRoutingModule
  ]
})
export class LandingModule { }
