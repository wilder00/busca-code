// general metatags
export const generalSeo = {
  //35-65 characters
  title: 'BuscaCode - LDOE',
  keywords: 'last day on earth, ldoe, herramientas en last day on earth, ayuda en last day on earth',
  robots: 'index, follow',
  author: 'Wilder Trujillo',
  viewport: 'width-device-width, initial-scale=1',
  date: '2022-04-28',
  dateSchema: 'YYYY-MM-DD',
  language: 'Spanish',
  //70-320 characters
  description: 'Last day on earth es un juego para moviles y tienen lugares como el bunker alfa el cual requiere de un código para abrirlo, si te olvidas, este sitio web te puede ayudar',
  url: 'https://www.buscacode.com'
}

//Open graph protocols 
export const ogSeo = {
  title: generalSeo.title,
  url: generalSeo.url, //url donde estará el metatag
  type: 'website',
  description: generalSeo.description,
  image: generalSeo.url + '/assets/images/buscacode_web.webp', //debería ser url de la imagen 
  siteName: 'BuscaCode',
}